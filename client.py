#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path) #Messages catalog of the client 
		if debug: #if we want to debug our programm 
			print(self.catalog) #print the messages catalog of the client
		self.r = None
		self.debug = debug
		self.protocol = defaultProtocol

	@classmethod 
	def withProtocol(cls, host):
		res = host.find("://") > 1 #if we found the str "://" in the host : it verify if we have a protocol
		return res 

	def completeUrl(self, host, route = ""): #this method allows to complete the URL
		if not Client.withProtocol(host): # we verify if we have a protocol for our host
			host = self.protocol + host #if not we add a protocol
		if route != "": #we verify if the given route is not empty
			route = "/"+route 
		return host+route #we return the URL completed

	#send an I said message to a host
	def say_something(self, host):
		m = Message() #create an object message 
		self.catalog.add_message(m) #add the message to the catalog
		route = self.completeUrl(host, m.content) #create the route 
		self.r = requests.post(route) #we send the data to the resource
		if self.debug: #if we use the debug mode 
			print("POST  "+route + "→" + str(self.r.status_code)) #we print the server answer to check that the request were treated
			print(self.r.text) #we print the request on a text format to know which request were sent
		return self.r.status_code == 201 #return the server status : 201 : OK, it means that the query works and a new ressource has been created

	#add to catalog all the covid from host server
	def get_covid(self, host):
		route = self.completeUrl(host,'/they-said')
		self.r = requests.get(route) #we get the data from the resource "route"
		res = self.r.status_code == 200 #return the server status : 200 : OK, it means that the query works
		if res: #if you suceed to get the data
			res = self.catalog.c_import(self.r.json()) #import the data in the catalog
		if self.debug: #if we want to debug our programm
			print("GET  "+ route + "→" + str(self.r.status_code)) #we print the server answer to check that the request were treated
			if res != False: #if the query does not work
				print(str(self.r.json())) #we print the request content in a json format (to know which request were sent)
		return res #we check that we imported the catalogue from the server

	#send to server list of I said messages
	def send_history(self, host):
		route = self.completeUrl(host,'/they-said')
		self.catalog.purge(Message.MSG_ISAID) #Delete messages from the catalog
		data = self.catalog.c_export_type(Message.MSG_ISAID) #we export messages to the catalog
		self.r = requests.post(route, json=data)
		if self.debug: #if we want to debug our programm
			print("POST  "+ route + "→" + str(self.r.status_code)) #we print the server answer to check that the request were treated
			print(str(data))  #we print what we sent (list of the messages ISAID)
			print(str(self.r.text)) #print the request content in a text format
		return self.r.status_code == 201 #we check that we created and sent the list of ISAID messages (201 Created)


if __name__ == "__main__":
	c = Client("client.json", True) #Create a client
	c.say_something("localhost:5000") #Send message to an host
	c.get_covid("localhost:5000") #Add all the covid case to the catalog on the host server localhost:5000
	c.send_history("localhost:5000") #Send a list of messages to server localhost:5000