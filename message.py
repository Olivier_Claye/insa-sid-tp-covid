#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "": #si mon message est vide, ca veut dire que je veux écrire un message, j'en génére donc un aléatoirement
			self.content = Message.generate() #le contenu du message est généré par la fonction generate()
			self.type = Message.MSG_ISAID #mon message est un message de type MSG_ISAID
			self.date = time.time() #je prends le temps actuel au moment de générer mon message
		elif msg_type is False : #j'ai un message, mais son type n'est pas renseigné
			self.m_import(msg) #je fais appel à la méthode m_import
		else: #si mon message a un contenu et qu'il possède un type
			self.content = msg #je récupère son contenu
			self.type = msg_type #je récupère son type
			if msg_date is False: #s'il n'a pas de date associée
				self.date = time.time() #je récupère la date actuelle à laquelle mon message est créé
			else: #s'il a une date associée
				self.date = msg_date#je récupère sa date de création

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date)

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date #to have my age, I have to substract the actual date (the one of today) and the date I was born (hear, the date the message was born). I will obtain an age in seconds
		if days: #if we want the message in days
			age = int(age/(3600*24)) #I divide my age in second by 3600*24 to have my age in days
		if as_string: #If I want to display my age in days, hours, minutes and seconds
			d = int(age/(3600*24))
			r = age%(3600*24) #I take the rest of the division : when we cannot have a full day
			h = int(r/3600) #I obtain the hours
			r = r%3600 #I take the rest : when we cannot have a full hour
			m = int(r/60) #I obtain the minute
			s = r%60 #I take the rest : when we cannot have a full minute
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")
		return age

	#testers of message type
	def is_i_said(self):
		return self.type == Message.MSG_ISAID #return true if the message is a MSG_ISAID
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD #return true if the message is a MSG_IHEARD
	def is_they_said(self):
		return self.type == Message.MSG_THEY #return true if the message is a MSG_THEY

	#setters
	def set_type(self, new_type):
		self.type = new_type #change the type of a message

	#a class method that generates a random message
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest()

	#a method to convert the object to string data
	def __str__(self):
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"

	#export/import
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"

	def m_import(self, msg):
		if(type(msg) == type(str())): #If my message is a string type
			json_object = json.loads(msg) #I use the json.load() method which takes a file object and returns the json object
		elif(type(msg) == type(dict())): #If my message is a dictionnary in json, which means an object in python
			json_object = msg 
		else:
			raise ValueError
		self.content = json_object["content"] #recupérer le contenu du message
		self.type = json_object["type"] #récupérer le type du message
		self.date = json_object["date"] #récupérer la date de création du message

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #Creation of a new message
	time.sleep(1)
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #Create a second message
	copyOfM = Message(myMessage.m_export()) #Create a third message
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5)
	print(copyOfM.age(True,True))
	time.sleep(0.5)
	print(copyOfM.age(False,True))
