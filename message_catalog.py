#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath):
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ]
		self.filepath = filepath #indique le chemin du fichier où sera placé le catalogue
		if os.path.exists(self.filepath): #if the file path is existing
			self.open() #you open the file path
		else: #if the file path is not existing
			self.save() #you save the filepath

	#destructor closes file.
	def __del__(self):
		del self.filepath #close the file
		del self.data #delete the data 

	#get catalog size
	def get_size(self, msg_type=False):
		if(msg_type is False): #if the message type is not informed
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) #we want the size of catalog incluiding all the differents types of messages
		else: #if we want one specific type of message
			res = len(self.data[msg_type]) #we just want the number of messages corresponding at 1 special type of message
		return res #the method return the catalog size

	#Import object content
	def c_import(self, content, save=True):
		i = 0
		for msg in content:
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #Add every message of the list 
					i=i+1
		if save:
			self.save() #We write the list of messages on a file
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY):
		i=0
		for msg in content: #for each message in the list content
			m = Message(msg)  #Create a new message
			m.set_type(new_type) #Set the type of this message
			if self.add_message(m, False): #If the message isn't already saved 
				i=i+1
		if i > 0:
			self.save() #Add the message to the catalog
		return i 

	#Open = load from file
	def open(self):
		file = open(self.filepath,"r") #Open a file 
		self.c_import(json.load(file), False) #We load the content of the file and import it 
		file.close() #We close the file 

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):
		tmp = int(bracket) * "[\n"
		first_item = True #Identifiate the first message
		for msg in self.data[msg_type]: 
			if first_item: #if the message is the first element
				first_item=False #the next messages won't be the first element of the list having the specific message type
			else:
				tmp = tmp +",\n" #if it is not the first message, we add a comma after the last adding
			tmp = tmp + (indent*" ") +  msg.m_export()  #we add a new message in the string tmp, according to the standard format of the export() method
		tmp = tmp + int(bracket) * "\n]"
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ): #Check if it's a "I said" message
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False) #Export the "I said" message
		if(len(self.data[Message.MSG_IHEARD]) > 0): #Check if it's a "I heard" message
			if tmp != "":
				tmp = tmp + ",\n" #Allow to return to new line if tmp is not empty
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False) #Export the "I heard" message
		if(len(self.data[Message.MSG_THEY]) > 0): #Check if it's a "They" message
			if tmp != "":
				tmp = tmp + ",\n" #Allow to return to new line if tmp is not empty
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False) #Export the "They" message
		tmp = "[" + tmp + "\n]" #Encapsulate the result into square brackets
		return tmp

	#a method to convert the object to string data
	def __str__(self):
		tmp="<catalog>\n"
		tmp=tmp+"\t<isaid>\n"
		for msg in self.data[Message.MSG_ISAID]: #for messages type MSG_ISAID
			tmp = tmp+str(msg)+"\n" #add the messages type MSG_ISAID in the string tmp, in a string format
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n" #close the "isaid" tag, and open a new tag for the messages type MSG_IHEARD
		for msg in self.data[Message.MSG_IHEARD]: #for messages type MSG_IHEARD
			tmp = tmp+str(msg)+"\n" #add the messages type MSG_IHEARD in the string tmp, in a string format
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n" #close the "iheard" tag, and open a new tag for the messages type MSG_THEY
		for msg in self.data[Message.MSG_THEY]: #for messages type MSG_THEY
			tmp = tmp+str(msg)+"\n" #add the messages type MSG_THEY in the string tmp, in a string format
		tmp=tmp+"\n\t</theysaid>\n</catalog>" #close the "theysaid" tag, and close the "catalog" tag
		return tmp

	#Save object content to file
	def save(self):
		file = open(self.filepath,"w") #Open the file 
		file.write(self.c_export()) #Write in this file the catalog content
		file.close() #Close the file 
		return True 

	#add a Message object to the catalog
	def add_message(self, m, save=True):
		res = True
		if(self.check_msg(m.content, m.type)): #check if the message is already present
			print(f"{m.content} is already there") #If yes we write it
			res = False 
		else:
			self.data[m.type].append(m) # If not, we add it to the list 
			if save:
				res = self.save() #We save that message 
		return res #We return if the operation succeed or not

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):
		if(msg_type is False):
			self.purge(max_age, Message.MSG_ISAID) 
			self.purge(max_age, Message.MSG_IHEARD)
			self.purge(max_age, Message.MSG_THEY)
		else:
			removable = []
			for i in range(len(self.data[msg_type])): #browse of the datas related to the message type in parameter
				if(self.data[msg_type][i].age(True)>max_age): 
					removable.append(i) #the removable table is the table which gather all the datas which are older than max_age 
			while len(removable) > 0: #while the table removable is not empty
					del self.data[msg_type][removable.pop()] #delete the values of removable in the specific msg_type
			self.save()
			for i in range(len(self.data[msg_type])):
				if(self.data[msg_type][i].age(True)>max_age):
					removable.append(i) #add every message to this table 
			while len(removable) > 0: #While this table is not empty
					del self.data[msg_type][removable.pop()] #We delete messages which correspond to the list "removable"
			self.save() #We actualise the catalog
		return True

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):
		for msg in self.data[msg_type]: #for all messages in our catalog and that have a MSG_THEY type
			if msg_str == msg.content: #Check if a message is already present in the list 
				return True #return true if the message already exist
		return False

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4):
		self.purge() #purge the catalog
		n = 0
		for msg in self.data[Message.MSG_IHEARD]: #For all messages in the data list and which correspond to type "IHEARD"
			if self.check_msg(msg.content): 
				n = n + 1  
		print(f"{n} covid messages heard")
		return max_heard < n #We return only if the number of message I heard is more than the max_heard

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json") #Create a catalog
	catalog.add_message(Message()) #Add a message to the catalog
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD)) #Add another message 
	print(catalog.c_export()) #Print all messages of the catalog in function of their type
	time.sleep(0.5) #wait
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}")) #Add a message with a type and a tim
	catalog.add_message(Message()) 
	print(catalog.c_export()) #Print this other exemple
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2) #delete all messages that exists since more than 2 days 
	print(catalog)
