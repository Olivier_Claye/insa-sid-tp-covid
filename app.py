#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET']) #If anyone use the path with a method 'GET'
def index():
	response = render_template("menu.html",root=request.url_root, h="") #Use of the template "menu.html" 
	return response

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) 
def add_heard(msg):
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)): #if the client add a message, the app has something to hear
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #the app is answering to the client if it had received a message
	else :
		reponse = Response(status=400) #the app answering that there is no message to hear 
	return response #Return the status of the message 
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST'])
def hospital():
	if request.method == 'GET': #If the methods used by the client is a GET one
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) #delete all the messages which type is MDG_THEY and which are older than 14 days
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #Status 200 means that the request succeed
	elif request.method == 'POST': #If it's a POST one
		if request.is_json: #the request sent by the client is in json
			req = json.loads(request.get_json()) #we load the resquest 
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY)
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201) #We add the "they said" messages to the server, the request status 201 means that this ressource has been created
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400) #if is not a request JSON, we significate tu user that his request need to be in JSON. Error 400 significate that the server does not understand the synthax request
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403) #In case of there is no method for the request
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET']) #A GET request for the path "app/check/<host>""
def check(host):
	h = host.strip() #delete characters spaces at the begining and at the end of the word
	n = client.get_covid(h) #add the host to the catalog of covid
	r = dict() #Create a dictionnary
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})" #We add a new line on the dictionnary with the status code of the message, id of host
	if client.catalog.quarantine(4): #Find if the client as to quarantine or not
		r["summary"] = "Stay home, you are sick." #we add to the dictionnary that the host has to stay home
	else:  #He does not need quarantine
		r["summary"] = "Everything is fine, but stay home anyway." #We add to the dictionnary that everything is fine
	return render_template("menu.html",responses=[r],root=request.url_root, h=h) #Return the menu with the dictionnary response, the root and host id 

@app.route('/declare/<host>', methods=['GET']) #A GET request for the path "/declare/<host>"
def declare(host):
	h = host.strip()#delete characters spaces at the begining and at the end of the word
	client.send_history(h) #send the host to the client server 
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})", #Text in a dictionnary to prevent that the client has the covid with his id, his status
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h) #Return the menu with the dictionnary response, the root and host id 

@app.route('/say/<hosts>', methods=['GET']) #A GET request for the path "/say/<hosts>"
def tell(hosts):
	hosts = hosts.split(",") #separe all host from de list
	r=[]
	for host in hosts:
		h = host.strip() #delete characters spaces at the begining and at the end of the word
		client.say_something(h) #send a message to the host with his id
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text}) #Add this message to a table
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging) #We create a new client
	app.run(host="0.0.0.0", debug=debugging) #Create a new host
