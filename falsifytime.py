#!/usr/bin/env python3
def falsifytime():
	import time as time
	#the referencetime is when we load this module
	referencetime = time.time()
	#factor is how many seconds of fake time correspond to a real second
	factor = 3600*24*14/60 #one real minute is fourteen days of fake time

	#override time.time()
	real_time = time.time # actual time
	def fake_time():
		duration = real_time() - referencetime #Calculate the duration of the message
		return referencetime + factor*duration #Convert the duration to days of fake time
	time.time = fake_time #replace the real time by the new fake time we've created
	return time #we return the new time that we will use to execute the others files



